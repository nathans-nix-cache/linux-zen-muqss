{
  description = "Linux Kernel caching";

  nixConfig = {
    extra-substituters = [ "https://nix-cache.mccarty.io/" ];
    extra-trusted-public-keys =
      [ "nathan-nix-cache:R5/0GiItBM64sNgoFC/aSWuAopOAsObLcb/mwDf335A=" ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, nixpkgs-unstable, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        unstable = import nixpkgs-unstable { inherit system; };
      in {
        packages = {
          linuxZenStable = pkgs.linuxPackages_zen.kernel.override {
            structuredExtraConfig = with pkgs.lib.kernel; {
              SCHED_MUQSS = yes;
            };
            ignoreConfigErrors = true;
          };
          linuxZenUnstable = unstable.linuxPackages_zen.kernel.override {
            structuredExtraConfig = with unstable.lib.kernel; {
              SCHED_MUQSS = yes;
            };
            ignoreConfigErrors = true;
          };
        };
      });
}
